# OpenML dataset: NHANES1_survival

https://www.openml.org/d/41439

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: National Center for Health Statistics
**Source**: [original](https://wwwn.cdc.gov/nchs/nhanes/nhanes1/Default.aspx) - Date unknown  
**Please cite**:   

Converted from output of https://github.com/slundberg/shap/blob/master/shap/datasets.py .

More info: 
https://wwwn.cdc.gov/nchs/nhanes/nhanes1/Default.aspx
https://wwwn.cdc.gov/nchs/nhanes/nhefs/publicuse.aspx

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41439) of an [OpenML dataset](https://www.openml.org/d/41439). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41439/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41439/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41439/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

